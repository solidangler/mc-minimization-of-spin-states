#include </home/solidangle/Code/MC Minimization of Spin States/spin-0.1.0.cpp>

int main()
{
SpinConfig sc;

sc.create_initial_spinset();

cout << "The first component of the first spin is: " << sc.spinset[0][0] << endl;

return 0;
}
